package com.viorn.aboutme.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.viorn.aboutme.R
import com.viorn.aboutme.viewmodel.AppsFragmentViewModel
import kotlinx.android.synthetic.main.fragment_apps.*
import kotlinx.android.synthetic.main.item_apps_list.view.*

/**
 * Created by viorn on 22.04.18.
 */

class AppsFragment : Fragment() {
    private val viewModel by lazy { ViewModelProviders.of(this).get(AppsFragmentViewModel::class.java).apply {
        lifecycle.addObserver(this)
    } }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_apps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        appList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        appList.adapter = adapter

        viewModel.appItems.observe(this, Observer {
            adapter.setData(it)
        })
    }

    private val adapter = object : RecyclerView.Adapter<ListItemHolder>() {
        val data = ArrayList<AppsFragmentViewModel.AppItem>()

        fun setData(data: List<AppsFragmentViewModel.AppItem>?) {
            this.data.clear()
            if (data != null)
                this.data.addAll(data)
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListItemHolder {
            return ListItemHolder(parent)
        }

        override fun getItemCount(): Int {
            return data.size
        }

        override fun onBindViewHolder(holder: ListItemHolder, position: Int) {
            holder.bind(data[position])
        }
    }

    inner class ListItemHolder(parent: ViewGroup?) :
            RecyclerView.ViewHolder(layoutInflater.inflate(R.layout.item_apps_list, parent, false)) {
        fun bind(item: AppsFragmentViewModel.AppItem) {
            itemView.name.text = item.name
            Picasso.get().cancelRequest(itemView.icon)
            Picasso.get().load(item.icon).into(itemView.icon)
        }
    }
}