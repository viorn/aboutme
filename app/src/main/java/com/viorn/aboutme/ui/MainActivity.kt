package com.viorn.aboutme.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import com.viorn.aboutme.R
import com.viorn.aboutme.viewmodel.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val viewModel by lazy { ViewModelProviders.of(this).get(MainActivityViewModel::class.java) }

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener {
        when (it.itemId) {
            R.id.action_about -> {
                supportFragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.in_left, R.anim.out_right)
                        .replace(R.id.mainFragmentContainer, AboutMeFragment())
                        .commit()
                title = "About me"
            }
            R.id.action_apps -> {
                supportFragmentManager.beginTransaction()
                        .apply {
                            if (navigationMenu.selectedItemId == R.id.action_about)
                                setCustomAnimations(R.anim.in_right, R.anim.out_left)
                            else
                                setCustomAnimations(R.anim.in_left, R.anim.out_right)
                            replace(R.id.mainFragmentContainer, AppsFragment())
                        }.commit()
                title = "Apps"
            }
            R.id.action_contact -> {
                supportFragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.in_right, R.anim.out_left)
                        .replace(R.id.mainFragmentContainer, ContactFragment())
                        .commit()
                title = "Contact"
            }
        }
        return@OnNavigationItemSelectedListener true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction()
                .replace(R.id.mainFragmentContainer, AboutMeFragment())
                .commit()

        navigationMenu.onNavigationItemSelectedListener = onNavigationItemSelectedListener
        navigationMenu.setOnNavigationItemReselectedListener {}
    }
}
