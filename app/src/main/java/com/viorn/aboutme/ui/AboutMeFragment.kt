package com.viorn.aboutme.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.viorn.aboutme.R
import com.viorn.aboutme.viewmodel.AboutMeFragmentViewModel
import kotlinx.android.synthetic.main.fragment_about_me.view.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch

/**
 * Created by viorn on 22.04.18.
 */

class AboutMeFragment : Fragment() {
    private val viewModel by lazy { ViewModelProviders.of(this).get(AboutMeFragmentViewModel::class.java) }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about_me, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.contentLiveData.observe(this, Observer {
            setContent(it)
        })
        viewModel.nameLiveData.observe(this, Observer {
            setName(it)
        })
    }

    private fun setContent(text: String?) = launch(UI) {
        view?.content?.text = text ?: ""
    }

    private fun setName(name: String?) = launch(UI) {
        view?.name?.text = name ?: ""
    }
}