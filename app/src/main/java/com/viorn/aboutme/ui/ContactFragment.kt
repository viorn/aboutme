package com.viorn.aboutme.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import com.viorn.aboutme.viewmodel.ContactFragmentViewModel

/**
 * Created by viorn on 22.04.18.
 */

class ContactFragment: Fragment() {
    val viewModel = lazy { ViewModelProviders.of(this).get(ContactFragmentViewModel::class.java) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
}