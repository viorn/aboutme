package com.viorn.aboutme.model.net

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.viorn.aboutme.model.entity.AppEntity
import kotlinx.coroutines.experimental.async
import okhttp3.OkHttpClient
import okhttp3.Request

class AppsRestControllerImpl(private val httpClient: OkHttpClient,
                             private val baseUrl: String,
                             private val gson: Gson) : AppsRestController {

    private fun buildGetAppsURL() = "$baseUrl/public/apps"

    override suspend fun getApps(): List<AppEntity> {
         return async {
            val request = Request.Builder().get().url(buildGetAppsURL()).build()
            val response = httpClient.newCall(request).execute()
                    .checkAndRefreshToken(httpClient)
                    .errorHanding()
            return@async gson.fromJson<List<AppEntity>>(response.body()?.string()!!, object : TypeToken<List<AppEntity>>() {}.type)
        }.await()
    }
}