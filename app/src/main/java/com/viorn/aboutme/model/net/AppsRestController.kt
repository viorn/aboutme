package com.viorn.aboutme.model.net

import com.viorn.aboutme.model.entity.AppEntity

interface AppsRestController {
    //public/apps
    suspend fun getApps(): List<AppEntity>
}