package com.viorn.aboutme.model.entity

data class AppEntity (
        var id: Int = -1,
        var name: String,
        var image: String? = null,
        var link: String,
        var description: String = ""
)