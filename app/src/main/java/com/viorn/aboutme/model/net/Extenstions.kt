package com.viorn.aboutme.model.net

import com.google.gson.reflect.TypeToken
import okhttp3.OkHttpClient
import okhttp3.Response
import java.lang.reflect.Type

fun Response.errorHanding(): Response{
    when (code()){

    }
    return this
}

fun Response.checkAndRefreshToken(httpClient: OkHttpClient): Response{
    if(code()==403){
        return httpClient.newCall(request()).execute()
    }
    return this
}