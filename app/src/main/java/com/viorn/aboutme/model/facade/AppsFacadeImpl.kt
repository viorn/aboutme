package com.viorn.aboutme.model.facade

import com.viorn.aboutme.model.entity.AppEntity
import com.viorn.aboutme.model.net.AppsRestController
import kotlinx.coroutines.experimental.async

class AppsFacadeImpl(private val appsRestController: AppsRestController) : AppsFacade {
    override suspend fun getAppList(): List<AppEntity>? {
        return async {
                return@async appsRestController.getApps()
        }.await()
    }
}