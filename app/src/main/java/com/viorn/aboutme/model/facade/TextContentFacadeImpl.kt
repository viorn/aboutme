package com.viorn.aboutme.model.facade

import com.viorn.aboutme.model.net.TextContentRestController
import kotlinx.coroutines.experimental.async

class TextContentFacadeImpl(private val textContentRestController: TextContentRestController) : TextContentFacade {
    override suspend fun getName(): String? {
        return async {
                return@async textContentRestController.getTextContent("name")
        }.await()
    }

    override suspend fun getAboutMeContent(): String? {
        return async {
               return@async textContentRestController.getTextContent("aboutMe")
        }.await()
    }
}
