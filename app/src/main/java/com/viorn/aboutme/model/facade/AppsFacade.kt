package com.viorn.aboutme.model.facade

import com.viorn.aboutme.model.entity.AppEntity

interface AppsFacade {
    suspend fun getAppList(): List<AppEntity>?
}