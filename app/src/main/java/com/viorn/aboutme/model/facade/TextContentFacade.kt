package com.viorn.aboutme.model.facade

interface TextContentFacade {
    suspend fun getAboutMeContent(): String?
    suspend fun getName(): String?
}