package com.viorn.aboutme.model.net

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.viorn.aboutme.model.tools.Log
import okhttp3.OkHttpClient
import okhttp3.internal.Util
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

val restModule = Kodein.Module {
    bind<OkHttpClient>() with singleton {
        OkHttpClient.Builder()
                /*.addInterceptor {
                    instance<Log>().debug("HTTP","${it.request().method()}::${it.request().url()}")
                    val response = it.proceed(it.request())
                    instance<Log>().debug("HTTP","${response.body()?.charStream()?.readText()}")
                    return@addInterceptor response
                }*/
                .build()
    }
    bind<Gson>() with singleton { GsonBuilder().create() }
    bind<String>("rootHttpURL") with provider { "http://192.168.1.201:8080/api" }
    bind<TextContentRestController>() with provider { TextContentRestControllerImpl(instance(), instance("rootHttpURL"), instance()) }
    bind<AppsRestController>() with provider { AppsRestControllerImpl(instance(), instance("rootHttpURL"), instance()) }
}