package com.viorn.aboutme.model.net

interface TextContentRestController {
    suspend fun getTextContent(key: String): String
}
