package com.viorn.aboutme.model.net

class HttpErrorException(public val code: Int, message: String = "Http status code $code"): Exception(message)
