package com.viorn.aboutme.model.tools

class LogImpl : Log {
    override fun debug(tag: String,text: String) {
        android.util.Log.d(tag, text)
    }
}