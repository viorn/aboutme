package com.viorn.aboutme.model.net

import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import okhttp3.OkHttpClient
import okhttp3.Request

class TextContentRestControllerImpl(private val httpClient: OkHttpClient,
                                    private val baseUrl: String,
                                    private val gson: Gson) : TextContentRestController {
    private fun buildTextContentURL(key: String) = "$baseUrl/public/textContent/$key"

    override suspend fun getTextContent(key: String): String {
        return async {
            val request = Request.Builder().get().url(buildTextContentURL(key)).build()
            val response = httpClient.newCall(request).execute()
                    .checkAndRefreshToken(httpClient)
                    .errorHanding()
            return@async response.body()?.string()!!
        }.await()
    }
}