package com.viorn.aboutme.model.facade

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider

val facadeModule = Kodein.Module{
    bind<TextContentFacade>() with provider { TextContentFacadeImpl(instance()) }
    bind<AppsFacade>() with provider { AppsFacadeImpl(instance()) }
}