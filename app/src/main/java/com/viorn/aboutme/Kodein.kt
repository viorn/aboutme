package com.viorn.aboutme

import com.viorn.aboutme.model.facade.facadeModule
import com.viorn.aboutme.model.net.restModule
import com.viorn.aboutme.model.tools.Log
import com.viorn.aboutme.model.tools.LogImpl
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton

val kodein = Kodein {
    bind<Log>() with singleton { LogImpl() }
    import(restModule)
    import(facadeModule)
}