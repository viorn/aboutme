package com.viorn.aboutme.viewmodel

import android.arch.lifecycle.*
import com.viorn.aboutme.kodein
import com.viorn.aboutme.model.facade.AppsFacade
import kotlinx.coroutines.experimental.launch
import org.kodein.di.generic.instance
import java.util.*

/**
 * Created by viorn on 22.04.18.
 */

class AppsFragmentViewModel : ViewModel(), LifecycleObserver {
    class AppItem(val id: Int,
                  val icon: String? = null,
                  val name: String = "")

    private val appsFacade by kodein.instance<AppsFacade>()

    val appItems = MutableLiveData<List<AppItem>>()

    init {
        launch { appItems.postValue(appsFacade.getAppList()?.map { AppItem(it.id, it.image, it.name) }) }
    }
}