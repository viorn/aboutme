package com.viorn.aboutme.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.viorn.aboutme.kodein
import com.viorn.aboutme.model.facade.TextContentFacade
import kotlinx.coroutines.experimental.launch
import org.kodein.di.generic.instance

/**
 * Created by viorn on 22.04.18.
 */

class AboutMeFragmentViewModel: ViewModel() {
    private val coreFacade by kodein.instance<TextContentFacade>()
    val contentLiveData by lazy { MutableLiveData<String?>() }
    val nameLiveData by lazy { MutableLiveData<String?>() }
    init {
        launch { contentLiveData.postValue(coreFacade.getAboutMeContent()) }
        launch { nameLiveData.postValue(coreFacade.getName()) }
    }
}